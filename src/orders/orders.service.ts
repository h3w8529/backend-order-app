import {
	Injectable,
	NotFoundException,
	Inject,
	Logger,
	BadGatewayException,
	BadRequestException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientProxy } from '@nestjs/microservices';
import { Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';
import * as moment from 'moment';

import { CreateOrderDto } from './dto/create-order-dto';
import { OrderRepository } from './order.repository';
import { Order } from './order.entity';
import { OrderStatus } from './order-status.enum';
import { CommonResponseDto } from './dto/common-response-dto';
import { CronExpression, Cron } from '@nestjs/schedule';

@Injectable()
export class OrdersService {
	private logger = new Logger('OrdersService');
	private orderToUpdate = [];

	constructor(
		@Inject('PAYMENT_APP_SERVICE') private readonly paymentService: ClientProxy,
		@InjectRepository(OrderRepository) private orderRepository: OrderRepository
	) {
		this.loadComfirmedButUndeliverOrder();
	}

	@Cron(CronExpression.EVERY_SECOND)
	handleCron(): void {
		const secondsOfTransferOrder = 60;
		// cron job to find the order that after x seconds and required flag it to delivered
		const orderToBeDeliver = this.orderToUpdate.filter((order) => {
			return moment.duration(moment().diff(moment(order.confirmedAt))).asSeconds() >= secondsOfTransferOrder;
		});

		this.orderToUpdate = this.orderToUpdate.filter((order) => {
			return moment.duration(moment().diff(moment(order.confirmedAt))).asSeconds() < secondsOfTransferOrder;
		});

		orderToBeDeliver.map(async (obj) => {
			obj.deliveredAt = new Date();

			if (!obj.paymentAt) {
				obj.paymentAt = obj.confirmedAt;
			}

			obj.status = OrderStatus.DELIVERED;
			await obj.save();
		});
	}

	private async loadComfirmedButUndeliverOrder() {
		this.orderToUpdate = (await this.getOrder({ page: 1, limit: 1000 }, OrderStatus.CONFIRMED)).items;
	}

	async getOrder(
		paginateOption?: IPaginationOptions,
		orderStatus?: OrderStatus,
		filterOrderNo?: string,
		filterRemark?: string,
		orderBy?: string,
		sortBy?: 'ASC' | 'DESC'
	): Promise<Pagination<Order>> {
		return await this.orderRepository.getOrder(
			paginateOption,
			orderStatus,
			filterOrderNo,
			filterRemark,
			orderBy,
			sortBy
		);
	}

	async getOrderById(id: string): Promise<Order> {
		const found = await this.orderRepository.findOne(id);

		if (!found) {
			throw new NotFoundException(`Order with the ID ${id} is not found.`);
		}
		return found;
	}

	async createOrder(createOrderDto: CreateOrderDto): Promise<Order> {
		return this.orderRepository.createOrder(createOrderDto);
	}

	async deleteOrder(id: string): Promise<void> {
		const result = await this.orderRepository.delete(id);

		if (result.affected === 0) {
			throw new NotFoundException(`Order with the ID ${id} is not found.`);
		}
	}

	async updateDocumentNoOrderStatus(id: string, status: OrderStatus, documentNo: string): Promise<Order> {
		const order = await this.getOrderById(id);
		order.status = status;
		order.orderNo = documentNo;

		if (OrderStatus.CANCELLED === status) {
			order.cancelledAt = new Date();
		}
		if (OrderStatus.CONFIRMED === status) {
			order.confirmedAt = new Date();

			this.orderToUpdate.push(order);
		}
		order.updatedAt = new Date();

		await order.save();
		return order;
	}

	async cancelOrder(id: string): Promise<Order> {
		const order = await this.getOrderById(id);

		if ([ OrderStatus.DELIVERED, OrderStatus.CANCELLED ].includes(order.status)) {
			throw new BadRequestException(
				`This order is already ${order.status.toLocaleLowerCase()} and cannot change the status to cancel.`
			);
		}

		order.status = OrderStatus.CANCELLED;
		order.cancelledAt = new Date();
		order.updatedAt = new Date();
		await order.save();
		return order;
	}

	async paymentRequest(order: Order): Promise<CommonResponseDto> {
		try {
			// hash key signature
			const signature = `${order.id}${order.status}${order.amount}`.split('-').join('').split('.').join('');

			const payload = {
				id: order.id,
				signature
			};

			this.paymentService.connect();

			const pattern = 'paymentRequest';
			return await this.paymentService.send(pattern, payload).pipe().toPromise<CommonResponseDto>();
		} catch (e) {
			throw new BadGatewayException('Payment App Server is not ready.');
		} finally {
			this.paymentService.close();
		}
	}
}
