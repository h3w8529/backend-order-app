import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule, Transport } from '@nestjs/microservices';
import * as config from 'config';

import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { OrderRepository } from './order.repository';
import { RunningNoRepository } from '../running_no/running_no.repository';
import { RunningNoService } from '../running_no/running_no.service';

const paymentAppConfig = config.get('payment-app-microservice');

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'PAYMENT_APP_SERVICE',
        transport: Transport.TCP,
        options: {
          host: paymentAppConfig.host,
          port: paymentAppConfig.port,
        },
      },
    ]),
    TypeOrmModule.forFeature([OrderRepository]),
    TypeOrmModule.forFeature([RunningNoRepository]),
  ],
  controllers: [OrdersController],
  providers: [OrdersService, RunningNoService],
})
export class OrdersModule {}
