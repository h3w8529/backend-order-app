import { Repository, EntityRepository } from 'typeorm';
import { paginate, Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';

import { Order } from './order.entity';
import { CreateOrderDto } from './dto/create-order-dto';
import { OrderStatus } from './order-status.enum';

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
	async getOrder(
		paginateOption?: IPaginationOptions,
		orderStatus?: OrderStatus,
		filterOrderNo?: string,
		filterRemark?: string,
		orderBy?: string,
		sortBy?: 'ASC' | 'DESC'
	): Promise<Pagination<Order>> {
		const query = this.createQueryBuilder('order');

		if (!!filterOrderNo) {
			query.andWhere('LOWER(order.orderNo) LIKE LOWER(:orderNo)', {
				orderNo: `%${filterOrderNo}%`
			});
		}

		if (!!filterRemark) {
			query.andWhere('LOWER(order.remark) LIKE (:remark)', {
				remark: `%${filterRemark}%`
			});
		}

		if (!!orderStatus) {
			query.andWhere('order.status = :status', { status: orderStatus });
		}

		if (!orderBy) {
			orderBy = 'createdAt';
		}

		if (!sortBy) {
			sortBy = 'DESC';
		}
		query.orderBy(`order.${orderBy}`, sortBy);
		return paginate<Order>(query, paginateOption);
	}

	async createOrder(createOrderDto: CreateOrderDto): Promise<Order> {
		const { remark, amount } = createOrderDto;
		const order = new Order();
		order.createdAt = new Date();
		order.updatedAt = new Date();
		order.remark = remark;
		order.amount = amount;
		await order.save();

		return order;
	}
}
