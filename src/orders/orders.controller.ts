import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  Param,
  Delete,
  Patch,
  UsePipes,
  ValidationPipe,
  ParseUUIDPipe,
  BadGatewayException,
} from '@nestjs/common';
import { Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';

import { RunningNoService } from 'src/running_no/running_no.service';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order-dto';
import { Order } from './order.entity';
import { OrderStatus } from './order-status.enum';

const route = 'orders';

@Controller(route)
export class OrdersController {
  constructor(
    private ordersService: OrdersService,
    private runningNoService: RunningNoService,
  ) {}

  @Get()
  getAllOrders(
    @Query('page') page = 1,
    @Query('limit') limit = 10,
    @Query('orderStatus')
    orderStatus?:
      | OrderStatus.CREATED
      | OrderStatus.CONFIRMED
      | OrderStatus.CANCELLED
      | OrderStatus.DELIVERED,
    @Query('filterOrderNo') filterOrderNo?: string,
    @Query('filterRemark') filterRemark?: string,
    @Query('orderBy') orderBy?: string,
    @Query('sortBy') sortBy?: 'ASC' | 'DESC',
  ): Promise<Pagination<Order>> {
    return this.ordersService.getOrder(
      <IPaginationOptions>{
        page,
        limit,
        route,
      },
      orderStatus,
      filterOrderNo,
      filterRemark,
      orderBy,
      sortBy,
    );
  }

  @Get('/:id')
  getOrderById(
    @Param('id', ParseUUIDPipe)
    id: string,
  ): Promise<Order> {
    return this.ordersService.getOrderById(id);
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createOrder(@Body() createOrderDto: CreateOrderDto): Promise<any> {
    let orderId = '';
    try {
      const order = await this.ordersService.createOrder(createOrderDto);
      orderId = order.id;

      const runningNo = await this.runningNoService.getRunningNo();

      // Payment Request from micro service;
      const paymentResponse = await this.ordersService.paymentRequest(order);
      const newOrderNumber = `${runningNo.prefix}${String(
        runningNo.nextNumber,
      ).padStart(runningNo.lengthOfDocument, '0')}`;

      /*********
      Update order status when
      payment response is success, mark order status to confirmed
      when payment response is unsuccess, mark order status to cancelled
      *********/
      await this.ordersService.updateDocumentNoOrderStatus(
        orderId,
        paymentResponse.c === 1 ? OrderStatus.CONFIRMED : OrderStatus.CANCELLED,
        newOrderNumber,
      );

      await this.runningNoService.updateDocumentNo(runningNo);

      return await this.ordersService.getOrderById(orderId);
    } catch (e) {
      await this.ordersService.deleteOrder(orderId);
      throw new BadGatewayException(`Payment-App is not running`);
    }
  }

  @Delete('/:id')
  deleteOrderById(
    @Param('id', ParseUUIDPipe)
    id: string,
  ): Promise<void> {
    return this.ordersService.deleteOrder(id);
  }

  @Patch('/:id/status')
  cancelOrderStatus(
    @Param('id', ParseUUIDPipe)
    id: string,
  ): Promise<Order> {
    return this.ordersService.cancelOrder(id);
  }
}
