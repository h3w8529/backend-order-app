import { Test } from '@nestjs/testing';

import { OrderRepository } from '../orders/order.repository';
import { OrdersService } from './orders.service';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { OrderStatus } from './order-status.enum';

const orderDetail = {
	id: 'ff1fc1dd-ad23-4d5d-9aed-b31867669463',
	orderNo: '#000008',
	remark: '12311',
	status: 'CONFIRMED',
	amount: '2.31',
	paymentAt: '2020-06-12T04:48:54.133Z',
	confirmedAt: '2020-06-12T04:48:54.603Z',
	cancelledAt: null,
	deliveredAt: '2020-06-12T04:49:05.003Z',
	createdAt: '2020-06-11T20:48:53.564Z',
	updatedAt: '2020-06-11T20:48:53.564Z',
	save: () => jest.fn()
};

const mockOrderRepository = () => ({
	findOne: jest.fn(),
	save: jest.fn(),
	delete: jest.fn()
});

describe('Order Service', () => {
	let orderRepository;
	let orderService;

	beforeEach(async () => {
		const module = await Test.createTestingModule({
			providers: [
				{
					provide: 'PAYMENT_APP_SERVICE',
					useValue: ''
				},
				OrdersService,
				{
					provide: OrderRepository,
					useFactory: mockOrderRepository
				}
			]
		}).compile();

		orderService = await module.get<OrdersService>(OrdersService);
		orderRepository = await module.get<OrderRepository>(OrderRepository);
	});

	describe('Get Order', () => {
		it('Get order by id', async () => {
			orderRepository.findOne.mockResolvedValue(orderDetail);
			const result = await orderService.getOrderById(orderDetail.id);
			expect(orderRepository.findOne).toHaveBeenCalled();
			expect(result).toEqual(orderDetail);
		});

		it('Throw an error as order is not found', () => {
			orderRepository.findOne.mockResolvedValue(null);
			expect(orderService.getOrderById(orderDetail.id)).rejects.toThrow(NotFoundException);
		});
	});

	describe('Update Document No and Order Status', () => {
		it('update document no and order status from created to confirm', async () => {
			const beforeUpdate = JSON.parse(JSON.stringify(orderDetail));
			beforeUpdate.orderNo = null;
			beforeUpdate.confirmedAt = null;
			beforeUpdate.status = OrderStatus.CREATED;
			beforeUpdate.save = () => jest.fn();
			orderRepository.findOne.mockResolvedValue(beforeUpdate);
			const result = await orderService.updateDocumentNoOrderStatus(
				beforeUpdate.id,
				OrderStatus.CONFIRMED,
				'#000001'
			);

			expect(result.orderNo).toEqual(beforeUpdate.orderNo);
			expect(result.status).toEqual(beforeUpdate.status);
			expect(result.confirmedAt).not.toBeNull();
		});

		it('update document no and order status from created to cancel', async () => {
			const beforeUpdate = JSON.parse(JSON.stringify(orderDetail));
			beforeUpdate.orderNo = null;
			beforeUpdate.cancelledAt = null;
			beforeUpdate.status = OrderStatus.CREATED;
			beforeUpdate.save = () => jest.fn();
			orderRepository.findOne.mockResolvedValue(beforeUpdate);
			const result = await orderService.updateDocumentNoOrderStatus(
				beforeUpdate.id,
				OrderStatus.CANCELLED,
				'#000001'
			);

			expect(result.orderNo).toEqual(beforeUpdate.orderNo);
			expect(result.status).toEqual(beforeUpdate.status);
			expect(result.cancelledAt).not.toBeNull();
		});

		it('Throw an error as order is not found', () => {
			orderRepository.findOne.mockResolvedValue(null);
			expect(orderService.getOrderById(orderDetail.id)).rejects.toThrow(NotFoundException);
		});
	});

	describe('Cancel Order', () => {
		it('Cancel order by id - with status of CREATED', async () => {
			const beforeCancel = JSON.parse(JSON.stringify(orderDetail));
			beforeCancel.status = OrderStatus.CREATED;
			beforeCancel.save = () => jest.fn();
			orderRepository.findOne.mockResolvedValue(beforeCancel);
			const afterCancel = JSON.parse(JSON.stringify(orderDetail));
			afterCancel.status = OrderStatus.CANCELLED;
			orderRepository.save.mockResolvedValue(afterCancel);
			const result = await orderService.cancelOrder(orderDetail.id);

			expect(result.status).toEqual(afterCancel.status);
		});

		it('Cancel order by id - with status of CONFIRMED', async () => {
			const beforeCancel = JSON.parse(JSON.stringify(orderDetail));
			beforeCancel.status = OrderStatus.CONFIRMED;
			beforeCancel.save = () => jest.fn();
			orderRepository.findOne.mockResolvedValue(beforeCancel);
			const afterCancel = JSON.parse(JSON.stringify(orderDetail));
			afterCancel.status = OrderStatus.CANCELLED;
			orderRepository.save.mockResolvedValue(afterCancel);
			const result = await orderService.cancelOrder(orderDetail.id);

			expect(result.status).toEqual(afterCancel.status);
		});

		it('Cancel order by id - with status of CANCELLED', async () => {
			const beforeCancel = JSON.parse(JSON.stringify(orderDetail));
			beforeCancel.status = OrderStatus.CANCELLED;
			beforeCancel.save = () => jest.fn();
			orderRepository.findOne.mockResolvedValue(beforeCancel);
			expect(orderService.cancelOrder(orderDetail.id)).rejects.toThrow(BadRequestException);
		});

		it('Cancel order by id - with status of DELIVERED', async () => {
			const beforeCancel = JSON.parse(JSON.stringify(orderDetail));
			beforeCancel.status = OrderStatus.DELIVERED;
			beforeCancel.save = () => jest.fn();
			orderRepository.findOne.mockResolvedValue(beforeCancel);
			expect(orderService.cancelOrder(orderDetail.id)).rejects.toThrow(BadRequestException);
		});
	});

	describe('Delete Order', () => {
		it('Delete order by id', async () => {
			orderRepository.delete.mockResolvedValue({ affected: 1 });
			expect(orderRepository.delete).not.toHaveBeenCalled();
			await orderService.deleteOrder(orderDetail.id);
			expect(orderRepository.delete).toHaveBeenCalledWith(orderDetail.id);
		});

		it('throws an error as order cound not found', async () => {
			orderRepository.delete.mockResolvedValue({ affected: 0 });
			expect(orderService.deleteOrder(orderDetail.id)).rejects.toThrow(NotFoundException);
		});
	});
});
