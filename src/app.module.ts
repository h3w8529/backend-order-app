import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

import { OrdersModule } from './orders/orders.module';
import { typeOrmConfig } from './config/typeorm.config';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot(typeOrmConfig),
    OrdersModule,
  ],
})
export class AppModule {}
