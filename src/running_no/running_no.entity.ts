import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class RunningNo extends BaseEntity {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column() category: string;

  @Column({ default: '#' })
  prefix: string;

  @Column({ default: 1 })
  nextNumber: number;

  @Column({ default: 6 })
  lengthOfDocument: number;

  @Column({ nullable: true, default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @Column({ nullable: true, default: () => 'CURRENT_TIMESTAMP' })
  updatedAt: Date;
}
