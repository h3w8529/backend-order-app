import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { RunningNoRepository } from './running_no.repository';
import { RunningNo } from './running_no.entity';

@Injectable()
export class RunningNoService {
  private logger = new Logger('RunningNoService');

  constructor(
    @InjectRepository(RunningNoRepository)
    private runningNoRepository: RunningNoRepository,
  ) {}

  async getRunningNo(category = 'OrderNo'): Promise<RunningNo> {
    const count = await this.runningNoRepository.count({ category });

    if (count === 0) {
      this.logger.log(
        `Running No Found, creating a new running no with category of ${category}`,
      );
      const runningNo = new RunningNo();
      runningNo.category = category;
      await runningNo.save();
    }

    return await this.runningNoRepository.findOne({ category });
  }

  async updateDocumentNo(runningNo: RunningNo): Promise<void> {
    runningNo.nextNumber = runningNo.nextNumber + 1;
    await runningNo.save();
  }
}
