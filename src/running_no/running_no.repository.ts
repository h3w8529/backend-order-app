import { Repository, EntityRepository } from 'typeorm';

import { RunningNo } from './running_no.entity';

@EntityRepository(RunningNo)
export class RunningNoRepository extends Repository<RunningNo> {}
